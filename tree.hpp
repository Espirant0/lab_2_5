#pragma once


namespace saw {
    struct Elem
    {
        int data;
        Elem* left;
        Elem* right;
        Elem* parent;
    };
    Elem* MAKE(int data, Elem* p);
    void ADD(int data, Elem*& root);
    void PASS(Elem* v);
    Elem* SEARCH(int data, Elem* v);
    void POSITION(int data, Elem* v, int k);
    void DELETE(int data, Elem*& root);
    void CLEAR(Elem*& v);

}